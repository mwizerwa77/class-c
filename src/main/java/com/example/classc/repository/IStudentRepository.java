package com.example.classc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.classc.model.Student;


@Repository
public interface IStudentRepository extends JpaRepository<Student, Long>{
	
	Student findByNames(String name);
}
