package com.example.classc.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.classc.model.Course;


@Repository
public interface ICourseRepository extends JpaRepository<Course, Long>{

	Optional<Course> findByCode(String courseCode);
	
}
