package com.example.classc.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.classc.model.Course;
import com.example.classc.model.Mark;
import com.example.classc.model.Student;


@Repository
public interface IMarkRepository extends JpaRepository<Mark, Long>{

	List<Mark> findAllByCourseAndStudent(Course course, Student student);

}
