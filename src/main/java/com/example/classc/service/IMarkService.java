package com.example.classc.service;

import com.example.classc.dto.CreateMarkDTO;
import com.example.classc.model.Course;
import com.example.classc.model.Mark;
import com.example.classc.model.Student;

public interface IMarkService{
	
	public Mark save(CreateMarkDTO dto, Student student, Course course);

	public Mark save(CreateMarkDTO dto, Student student, Course course,Long id);

}
