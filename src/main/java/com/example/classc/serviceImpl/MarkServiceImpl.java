package com.example.classc.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.classc.dto.CreateMarkDTO;
import com.example.classc.model.Course;
import com.example.classc.model.Mark;
import com.example.classc.model.Student;
import com.example.classc.repository.IMarkRepository;
import com.example.classc.service.IMarkService;

@Service
public class MarkServiceImpl implements IMarkService {
	
	@Autowired
	private IMarkRepository markRepository;
	
	@Override
	public Mark save(CreateMarkDTO dto, Student student, Course course) {
		Mark mark = new Mark();

		mark.setStudent(student);

		mark.setCourse(course);

		mark.setMax(dto.getMax());
		mark.setScored(dto.getScored());
 
		markRepository.save(mark);
		return mark;
	}

	@Override
	public Mark save(CreateMarkDTO dto, Student student, Course course, Long id) {
		Mark mark = markRepository.findById(id).get();

		mark.setStudent(student);

		mark.setCourse(course);

		mark.setMax(dto.getMax());
		mark.setScored(dto.getScored());
		
		markRepository.save(mark);
		
		return mark;
	}

}
