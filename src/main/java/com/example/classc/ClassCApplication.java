package com.example.classc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClassCApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClassCApplication.class, args);
	}

}
