/**
 * 
 */
package com.example.classc.enums;

/**
 * @author Stanley
 *
 */
public enum EAccountStatus {
	PENDING, ACTIVE, DISABLED, SUSPENDED, EXPIRED, RESET
}
